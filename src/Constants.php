<?php
const IMPORT_DIRECTORY = __DIR__ . '/../imports/';
const EXPORT_DIRECTORY = __DIR__ . '/../exports/';

define('DUMP_DATE', ! empty($argv[1]) ? $argv[1] : null);
define('FINANCE_CHECK', ! empty($argv[2]) ? $argv[2] : null);


const FILTER_BOOKINGS = true;

const BRAND_NAME = 'nccc';

const FINANCE_CHECK_LIST = [1, 2, 3, 4, 5, 6, 7, 8];

const FINANCIAL_YEAR_START_DATE = '2022-10-01';

const KNOWN_BOOKINGS_FILE = '../known.csv';
const ERROR_CHECK_FILE    = __DIR__ . '/../error_check.csv';


const HEADERS_FILTERED_BOOKINGS = [
    'Booking Ref',
    'Total Cost',
    'Total Rent',
    'Booking Fee',
    'Extras',
    'Due to owner RENT',
    'Due to owner EXTRAS',
    'Total Due to owner',
    'Paid to owner',
    'OWNER LIABILITY',
    'Total to NCCC',
    'Comm Gross',
    'Comm Net',
    'Comm VAT',
    'Extras Comm Gross',
    'Extras Comm Net',
    'Extras Comm VAT',
    'Total Received From Customer',
    'Total Relating to Holiday',
    'Balance Due on Holiday',
    'SD Amount',
    'SD Paid',
    'SD Refunded',
    'Balance of SD Held',
    'Status',
    'Property ref',
    'Owner ref',
    'Holiday booked date',
    'Holiday start date',
    'Holiday end date',
    'Cancelled Date',
    'Cancellation Rate',
    'Source',
    'Comm%',
    'Property Comm%',
    'Property Name',
    'Property Unique Ref',
    'Discount',
    'Voucher',
    'Booking Type',
    'Insurance',
    'Reclaimed From Owner',
    'Paid Per Statement',
    'Compensation/Reversal/OwnerCancellationFee',
    'Comments',
    'Cleaner Notes',
];

const FINANCE_CHECK_HEADERS = [
    'Check 1',
    'Check 2',
    'Check 3',
    'Check 4',
    'Check 5',
    'Check 6',
    'Check 7',
    'Check 8',
    'Check 9',
    'Check 10',
];

const FINANCE_CHECK_HEADER_DESCRIPTIONS = [
    'Check 1'  => 'Total Cost - Total Rent - Booking Fee - Extras',
    'Check 2'  => 'Total Cost - Total Due to owner - Total to NCCC',
    'Check 3'  => 'Total Due to owner - Due to owner EXTRAS - Due to owner RENT',
    'Check 4'  => 'Total to NCCC - Comm Gross - Booking Fee - Extras Comm Gross',
    'Check 5'  => 'Total Rent - Due to owner RENT - (Comm Gross)',
    'Check 6'  => 'Extras - Due to owner EXTRAS - Extras Comm Gross',
    'Check 7'  => 'Total Due to owner - Paid to owner - OWNER LIABILITY',
    'Check 8'  => 'Total Cost - Total Relating to Holiday - Balance Due on Holiday',
    'Check 9'  => 'Property Comm% - Comm%',
    'Check 10' => 'Paid to owner - Paid Per Statement',
];

const DODGY_BOOKINGS = [
    //Bookings failing Test 1

    //Bookings failing Test 2
    239342,
    245553, // Owner Invoice does not include the Admin Fee
    271110, // Fully refunded but not zeroed out
    274453, // Not fully refunded and not zeroed out
    274670, // Not zeroed out
    276443, // Not zeroed out

    //Bookings failing Test 3

    //Bookings failing Test 4

    //Bookings failing Test 5

    //Bookings failing Test 6

    //Bookings failing Test 7

    //Bookings failing Test 8
];