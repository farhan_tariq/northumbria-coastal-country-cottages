<?php
require_once 'base.php';

foreach ($bookings as $booking) {
    $sdPaid     = $booking->security_deposit_paid_amount;
    $sdRefunded = getSecurityDepositRefunded($booking);
    $sdHeld     = $sdPaid > 0 && $sdRefunded == 0 ? $sdPaid : round($sdPaid - $sdRefunded, 2);

    $totalReceivedFromCustomer = round($booking->total_received_from_customer - $booking->total_refunded_to_customer, 2);
    $totalRelatingToHoliday    = round($totalReceivedFromCustomer, 2);

    $rentalPrice = getRentalPrice($booking);
    $ownerExtras = getOwnerExtrasTotal($booking);
    $extrasTotal = getExtrasCommGross($booking);
    $bookingFee  = getBookingFee($booking);
    $insurance   = getBookingInsurance($booking);

    $booking->ncc_extras_total   = $extrasTotal;
    $booking->owner_extras_total = $ownerExtras;
    $booking->booking_fee        = $bookingFee;
    $booking->insurance          = $insurance;

    $totalPrice         = $rentalPrice + $extrasTotal + $ownerExtras + $bookingFee + $insurance;
    $reclaimedFromOwner = getSecurityDepositFeeClaimedFromOwner($booking) * -1;
    $totalDueToOwner    = $booking->due_to_owner + $reclaimedFromOwner;
    $paidToOwner        = getAmountPaidToOwnerPerBookingInterface($booking);
    $ownerLiability     = round($totalDueToOwner - $paidToOwner, 2);

    if ($totalReceivedFromCustomer > $totalPrice) {
        $totalRelatingToHoliday -= $sdHeld;
    }

    $balanceDueOnHoliday            = round($totalPrice - $totalRelatingToHoliday, 2);
    $totalToNCCC                    = getTotalToNCCC($booking);
    $calculatedCommissionPercentage = (getPropertyCommissionPercentage($booking) * 100) / 1.2;

    $row = [
        $booking->id,                                // 'Booking Ref'
        $totalPrice,                                 // 'Total Cost'
        $rentalPrice,                                // 'Total Rent'
        $bookingFee,                                 // 'Booking Fee'
        $extrasTotal + $ownerExtras,                 // 'Extras'
        $totalDueToOwner - $ownerExtras,             // 'Due to owner RENT'
        $ownerExtras,                                // 'Due to owner EXTRAS'
        $totalDueToOwner,                            // 'Total Due to owner'
        $paidToOwner,                                // 'Paid to owner'
        $ownerLiability,                             // 'OWNER LIABILITY'
        $totalToNCCC,                                // 'Total to NCCC'
        getCommissionGross($booking),                // 'Comm Gross'
        getCommissionNet($booking),                  // 'Comm Net'
        getCommissionVat($booking),                  // 'Comm VAT'
        $extrasTotal,                                // 'Extras Comm Gross'
        getExtrasCommNet($booking),                  // 'Extras Comm Net'
        getExtrasCommVAT($booking),                  // 'Extras Comm VAT'
        $totalReceivedFromCustomer,                  // 'Total Received From Customer'
        $totalRelatingToHoliday,                     // 'Total Relating to Holiday'
        $balanceDueOnHoliday,                        // 'Balance Due on Holiday'
        $booking->security_deposit_amount,           // 'SD Amount'
        $sdPaid,                                     // 'SD Paid'
        $sdRefunded,                                 // 'SD Refunded'
        $sdHeld,                                     // 'Balance of SD Held'
        $booking->status,                            // 'Status'
        $booking->_fk_property,                      // 'Property ref'
        $booking->_fk_owner,                         // 'Owner ref'
        $booking->booked_date,                       // 'Holiday booked date'
        $booking->from_date,                         // 'Holiday start date'
        $booking->to_date,                           // 'Holiday end date'
        $booking->cancelled_date,                    // 'Cancelled Date'
        $booking->cancelled_bookingfullrate,         // 'Cancellation Rate'
        $booking->source,                            // 'Source'
        $calculatedCommissionPercentage,             // 'Comm%'
        $booking->property_commission_percentage,    // 'Property Comm%'
        $booking->property_name,                     // 'Property Name'
        $booking->property_unique_ref,               // 'Property Unique Ref'
        $booking->discount,                          // 'Discount'
        $booking->voucher,                           // 'Voucher'
        $booking->booking_type,                      // 'Booking Type'
        $insurance,                                  // 'Insurance'
        $reclaimedFromOwner,                         // 'Reclaimed From Owner'
        getAmountPaidToOwnerPerStatement($booking),  // 'Paid Per Statement'
        $booking->owner_payments_adjusted_amount,    // 'Compensation/Reversal/OwnerCancellationFee'
        stripTags($booking->comment),                // 'Comment'
        stripTags($booking->cleanersnotes),          // 'Cleaner Notes'
    ];

    $row = array_combine(HEADERS_FILTERED_BOOKINGS, $row);

    $financeChecks->printBooking($booking, $row);
    $financeChecks->runFinanceChecks($booking, $row, $sdPaid, $sdHeld, $sdRefunded);
    $financeChecks->sumFinanceChecks($row);
    $financeChecks->checkBooking($known, $booking, $row);

    $financeChecksResults = $financeChecks->getBookingsErrorCheck()[$booking->id];
    $financeChecksRow     = array_combine(FINANCE_CHECK_HEADERS, (array) $financeChecksResults);
    $row                  = array_merge($row, $financeChecksRow);

    if (empty(FINANCE_CHECK)) {
        fputcsv($out, $row);
    }
}

if (empty(FINANCE_CHECK)) {
    fclose($out);
    echo 'Saved file: ' . $outputFileName . PHP_EOL . PHP_EOL;
    $financeChecks->compareFinanceChecks();
    $financeChecks->appendDataToErrorCheck(ERROR_CHECK_FILE);
}

//$financeChecks->outputErrorReport('check8');

function getRentalPrice($booking)
{
    $rental = $booking->adjusted_rent;

    if (strtolower($booking->status) !== 'confirmed') {
        if ($booking->adjusted_rent > 0) {
            $rental = $booking->adjusted_rent;
        } else {
            $totalPaidByCustomer = $booking->total_received_from_customer - $booking->total_refunded_to_customer;
            $rental              = $totalPaidByCustomer > 0 ? $booking->cancelled_bookingfullrate : $rental;
        }
    }

    $booking->calculated_rental = $rental;
    return $rental;

}

function getTotalToNCCC($booking): float
{
    return round((getBookingCommission($booking) + $booking->booking_fee + $booking->insurance + getExtrasCommGross($booking)), 2);
}

function getCommissionNet($booking): float
{
    return round((getBookingCommission($booking)) / 1.2, 2);
}

function getCommissionVAT($booking): float
{
    return round(getCommissionNet($booking) * 0.2, 2);
}

function getCommissionGross($booking): float
{
    return getCommissionNet($booking) + getCommissionVat($booking);
}

function getExtrasCommNet($booking): float
{
    $total = 0;
    foreach ($booking->extras as $extra) {
        if (
            (
                (int) $extra->ownerprice === 0 &&
                (int) $extra->extra_type !== 13304 &&
                (int) $extra->extra_type !== 13305 &&
                ! array_stripos($extra->description, ['hcp'])
            ) ||
            array_stripos($extra->description, ['admin', 'amin'])
        ) {
            $total += $extra->total_price;
        }
    }

    return $total / 1.2;
}

function getExtrasCommVAT($booking): float
{
    $extrasCommNet = getExtrasCommNet($booking);

    return round($extrasCommNet * 1.2 - $extrasCommNet, 2);
}

function getExtrasCommGross($booking): float
{
    return round(getExtrasCommNet($booking) + getExtrasCommVAT($booking), 2);
}

function getOwnerExtrasTotal($booking): float
{
    $total = 0;
    foreach ($booking->extras as $extra) {
        if (
            ((int) $extra->ownerprice > 0 || (int) $extra->ownerprice < 0) &&
            ! array_stripos($extra->description, ['admin', 'amin'])
        ) {
            $total += $extra->ownerprice;
        }
    }

    return $total;
}

function getSecurityDepositFeeClaimedFromOwner($booking): float
{
    $total = 0;
    foreach ($booking->owner_payments as $owner_payment) {
        if (strripos('- ' . $owner_payment->payment_caption, 'securi')) {
            $total += $owner_payment->amount;
        }
    }
    return $total;
}

function getSecurityDepositRefunded($booking): float
{
    $refundAmount = 0;
    $sdPayment    = null;
    if ($booking->security_deposit_refunded == 0 && $booking->security_deposit_amount > 0) {
        foreach ($booking->payments as $payment) {
            if (array_stripos($payment->paymentcaption, ['securi', 'sd']) !== false) {
                if ($payment->amount > 0) {
                    $sdPayment = $payment;
                    continue;
                }
            }

            if ($sdPayment !== null) {
                if (
                    $payment->amount < 0 &&
                    $payment->date >= $sdPayment->date &&
                    $sdPayment->amount == abs($payment->amount)
                ) {
                    $refundAmount += $sdPayment->amount;
                }
            }
        }
    }

    return $refundAmount + $booking->security_deposit_refunded;
}

function getPropertyCommissionPercentage($booking): float
{
    $propertyCommissionPercentage = (float) $booking->property_commission_percentage;
    $rentalPrice                  = $booking->calculated_rental;

    $bookingCommissionWithVAT = getBookingCommission($booking);

    if ($rentalPrice > 0) {
        if ($bookingCommissionWithVAT / $rentalPrice === $propertyCommissionPercentage) {
            return $propertyCommissionPercentage / 100;
        }

        return ($bookingCommissionWithVAT / $rentalPrice);
    }

    return $propertyCommissionPercentage / 100;
}

function getBookingCommission($booking): float
{
    $total = 0;
    foreach ($booking->owner_payments as $owner_payment) {
        $total += $owner_payment->commission_amount;
    }

    return $total;
}

function getBookingFee($booking): float
{
    $total = 0;
    foreach ($booking->extras as $extra) {
        if ((int) $extra->extra_type === 13304) {
            $total += $extra->total_price;
        }
    }

    return $total;
}

function getBookingInsurance($booking): float
{
    $total = 0;
    foreach ($booking->extras as $extra) {
        if (array_stripos($extra->description, ['insurance', 'protec', 'hcp'])) {
            $total += $extra->total_price;
        }
    }

    return $total;
}

function getAmountPaidToOwnerPerStatement($booking)
{
    $total = 0;

    foreach ($booking->owner_payments as $payment) {
        if ($payment->paid_date <= DUMP_DATE && $payment->paid_date !== '00-00-00 00:00:00' && $payment->paid_date !== null) {
            $total += $payment->amount;
        }
    }
    foreach ($booking->owner_payment_adjustments as $payment) {
        if ($payment->paid_date <= DUMP_DATE && $payment->paid_date !== '00-00-00 00:00:00' && $payment->paid_date !== null) {
            $total += $payment->amount;
        }
    }

    return $total;
}

function getAmountPaidToOwnerPerBookingInterface($booking)
{
    $total = 0;

    foreach ($booking->owner_payments as $payment) {
        if ($payment->paid_date <= DUMP_DATE && $payment->paid_date !== '00-00-00 00:00:00' && $payment->paid_date !== null) {
            $total += $payment->amount;
        }
    }

    return $total;
}
