<?php

class FinanceChecks
{
    public object $currentErrorCheck;
    public array $bookingsErrorCheck;
    public object $totalErrorCheck;
    public object $bookingErrorCheck;
    public object $lastErrorCheck;

    public const FINANCE_CHECK_THRESHOLD = 0.05;

    public function __construct()
    {
        $this->currentErrorCheck  = $this->emptyErrorCheckObject();
        $this->totalErrorCheck    = $this->emptyErrorCheckObject();
        $this->bookingErrorCheck  = $this->emptyBookingErrorCheckObject();
        $this->lastErrorCheck     = $this->readLatestFinanceErrorCheck();
        $this->bookingsErrorCheck = [];
    }

    private function emptyErrorCheckObject(): object
    {
        return (object) [
            'check1'    => 0,
            'check2'    => 0,
            'check3'    => 0,
            'check4'    => 0,
            'check5'    => 0,
            'check6'    => 0,
            'check7'    => 0,
            'check8'    => 0,
            'checkDate' => date('Y-m-d H:i'),
        ];
    }

    private function emptyBookingErrorCheckObject(): object
    {
        return (object) [
            'check1'    => [],
            'check2'    => [],
            'check3'    => [],
            'check4'    => [],
            'check5'    => [],
            'check6'    => [],
            'check7'    => [],
            'check8'    => [],
            'checkDate' => date('Y-m-d H:i'),
        ];
    }

    private function readLatestFinanceErrorCheck(): object
    {
        $financeErrorChecks = fopen(ERROR_CHECK_FILE, 'rb');

        if (! $financeErrorChecks) {
            die('Could not open file: "' . ERROR_CHECK_FILE . '"');
        }

        $headers = fgetcsv($financeErrorChecks);

        $lastErrorCheck = [];
        while (($row = fgetcsv($financeErrorChecks)) !== false) {
            $row            = (object) array_combine($headers, $row);
            $lastErrorCheck = $row;
        }

        if (empty($lastErrorCheck)) {
            $lastErrorCheck = $this->currentErrorCheck;
        }

        return $lastErrorCheck;
    }

    public function financeCheck($checkSum): bool
    {
        return abs($checkSum) < self::FINANCE_CHECK_THRESHOLD;
    }

    public function runFinanceChecks($booking, $row, $sdPaid, $sdHeld, $sdRefunded): void
    {
        if (! empty(FINANCE_CHECK) && ! in_array((int) $booking->id, DODGY_BOOKINGS, true)) {
            $index = (int) FINANCE_CHECK;

            // Total Cost - Rent +Extras + Bk Fee + Charity Donation
            if ($index === 1) { // =B3-C3-D3-E3-F3
                $financeCheck = ($row['Total Cost'] - $row['Total Rent'] - $row['Booking Fee'] - $row['Extras'] - $row['Insurance']);
                $this->financeCheckOne($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded);
            }

            // Total Cost - Total To owner + Total to NCCC
            if ($index === 2) { // =B3-J3-M3
                $financeCheck = ($row['Total Cost'] - $row['Total Due to owner'] - $row['Total to NCCC']);
                $this->financeCheckTwo($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded);
            }

            // Owner Total - Rent + Extras
            if ($index === 3) { // =J3-I3-H3
                $financeCheck = ($row['Total Due to owner'] - $row['Due to owner EXTRAS'] - $row['Due to owner RENT']);
                $this->financeCheckThree($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded);
            }

            // NCCC Total - Comm Rent + Comm Extras + Bk Fee
            if ($index === 4) { // =M3-N3-Q3-T3
                $financeCheck = ($row['Total to NCCC'] - $row['Comm Gross'] - $row['Booking Fee'] - $row['Extras Comm Gross'] - $row['Insurance']);
                $this->financeCheckFour($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded);
            }

            // Rent - Owner Rent + Comm Rent
            if ($index === 5) { // =C3-H3-N3
                $financeCheck = ($row['Total Rent'] - $row['Due to owner RENT'] - ($row['Comm Gross']));
                $this->financeCheckFive($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded);
            }

            // Total Extras - Owner Extras - NCCC Extras
            if ($index === 6) { // =E3-I3-Q3
                $financeCheck = ($row['Extras'] - $row['Due to owner EXTRAS'] - $row['Extras Comm Gross']);
                $this->financeCheckSix($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded);
            }

            // Total to Owner - Total Paid -Total Due
            if ($index === 7) { // =J3-K3-L3
                $financeCheck = ($row['Total Due to owner'] - $row['Paid to owner'] - $row['OWNER LIABILITY']);
                $this->financeCheckSeven($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded);
            }

            // Total Cost - Received from Customer - Balance due
            if ($index === 8) { // =B3-V3-X3
                $financeCheck = $row['Total Cost'] - $row['Total Relating to Holiday'] - $row['Balance Due on Holiday'];
                $this->financeCheckEight($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded);
            }
        }
    }

    public function financeCheckOne($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded): void
    {
        // skip when difference is the same amount as the voucher

        if (! $this->financeCheck($financeCheck)) {
            print_r($row);
            print_r($booking);

            var_export(
                [
                    'Check 01 failed: ' => $booking->id,
                    'sdPaid'            => $sdPaid,
                    'sdHeld'            => $sdHeld,
                    'sdRefunded'        => $sdRefunded,
                    'financeCheck'      => $financeCheck,
                    'check FORMULA'     => '($row["Total Cost"] - $row["Total Rent"] - $row["Booking Fee"] - $row["Extras"] - $row["Insurance"])',
                    'Total Cost'        => $row['Total Cost'],
                    'Total Rent'        => (float) $row['Total Rent'],
                    'Booking Fee'       => $row['Booking Fee'],
                    'Insurance'         => $row['Insurance'],
                    'Extras'            => $row['Extras'],

                    'Booking Status' => $row['Status'],
                ]
            );
            die;
        }
    }

    public function financeCheckTwo($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded): void
    {
        if (! $this->financeCheck($financeCheck)) {
            print_r($row);
            print_r($booking);

            var_export(
                [
                    'Check 02 failed: '  => $booking->id,
                    'sdPaid'             => $sdPaid,
                    'sdHeld'             => $sdHeld,
                    'sdRefunded'         => $sdRefunded,
                    'financeCheck'       => $financeCheck,
                    'check FORMULA'      => '$row["Total Cost"] - $row["Total Due to owner"] - $row["Total to NCCC"]',
                    'Total Cost'         => $row['Total Cost'],
                    'Total Due to owner' => $row['Total Due to owner'],
                    'Total to NCCC'      => $row['Total to NCCC'],

                    'Booking Status' => $row['Status'],
                ]
            );
            die;
        }
    }

    public function financeCheckThree($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded): void
    {
        if (! $this->financeCheck($financeCheck)) {
            print_r($row);
            print_r($booking);

            var_export(
                [
                    'Check 03 failed: '   => $booking->id,
                    'sdPaid'              => $sdPaid,
                    'sdHeld'              => $sdHeld,
                    'sdRefunded'          => $sdRefunded,
                    'financeCheck'        => $financeCheck,
                    'check FORMULA'       => '$row["Total Due to owner"] - $row["Due to owner EXTRAS"] - $row["Due to owner RENT"]',
                    'Total Due to owner'  => $row['Total Due to owner'],
                    'Due to owner EXTRAS' => $row['Due to owner EXTRAS'],
                    'Due to owner RENT'   => $row['Due to owner RENT'],

                    'Booking Status' => $row['Status'],
                ]
            );
            die;
        }
    }

    public function financeCheckFour($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded): void
    {
        if (! $this->financeCheck($financeCheck)) {
            print_r($row);
            print_r($booking);

            var_export(
                [
                    'Check 04 failed: ' => $booking->id,
                    'sdPaid'            => $sdPaid,
                    'sdHeld'            => $sdHeld,
                    'sdRefunded'        => $sdRefunded,
                    'financeCheck'      => $financeCheck,
                    'check FORMULA'     => '$row["Total to NCCC"] - $row["Comm Gross"] - $row["Booking Fee"] - $row["Extras Comm Gross"] - $row["Insurance"]',
                    'Total to NCCC'     => $row['Total to NCCC'],
                    'Comm Gross'        => $row['Comm Gross'],
                    'Booking Fee'       => $row['Booking Fee'],
                    'Extras'            => $row['Extras'],
                    'Insurance'         => $row['Insurance'],

                    'Booking Status' => $row['Status'],
                ]
            );
            die;
        }
    }

    public function financeCheckFive($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded): void
    {
        if (! $this->financeCheck($financeCheck)) {
            print_r($row);
            print_r($booking);

            var_export(
                [
                    'Check 05 failed: ' => $booking->id,
                    'sdPaid'            => $sdPaid,
                    'sdHeld'            => $sdHeld,
                    'sdRefunded'        => $sdRefunded,
                    'financeCheck'      => $financeCheck,
                    'check FORMULA'     => '$row["Total Rent"] - $row["Due to owner RENT"] - ($row["Comm Gross"])',
                    'Total Rent'        => $row['Total Rent'],
                    'Due to owner RENT' => $row['Due to owner RENT'],
                    'Comm Gross'        => $row['Comm Gross'],

                    'Booking Status' => $row['Status'],
                ]
            );
            die;
        }
    }

    public function financeCheckSix($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded): void
    {
        if (! $this->financeCheck($financeCheck)) {
            print_r($row);
            print_r($booking);

            var_export(
                [
                    'Check 06 failed: '   => $booking->id,
                    'sdPaid'              => $sdPaid,
                    'sdHeld'              => $sdHeld,
                    'sdRefunded'          => $sdRefunded,
                    'financeCheck'        => $financeCheck,
                    'check FORMULA'       => '$row["Extras"] - $row["Due to owner EXTRAS"] - $row["Extras Comm Gross"]',
                    'Extras'              => $row['Extras'],
                    'Due to owner EXTRAS' => $row['Due to owner EXTRAS'],
                    'Extras Comm Gross'   => $row['Extras Comm Gross'],

                    'Booking Status' => $row['Status'],
                ]
            );
            die;
        }
    }

    public function financeCheckSeven($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded): void
    {
        if (! $this->financeCheck($financeCheck)) {
            print_r($row);
            print_r($booking);

            var_export(
                [
                    'Check 07 failed: '  => $booking->id,
                    'sdPaid'             => $sdPaid,
                    'sdHeld'             => $sdHeld,
                    'sdRefunded'         => $sdRefunded,
                    'financeCheck'       => $financeCheck,
                    'check FORMULA'      => '$row["Total Due to owner"] - $row["Paid to owner"] - $row["OWNER LIABILITY"]',
                    'Total Due to owner' => $row['Total Due to owner'],
                    'Paid to owner'      => $row['Paid to owner'],
                    'OWNER LIABILITY'    => $row['OWNER LIABILITY'],

                    'Booking Status' => $row['Status'],
                ]
            );
            die;
        }
    }

    public function financeCheckEight($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded): void
    {
        if (! $this->financeCheck($financeCheck)) {
            print_r($row);
            print_r($booking);

            var_export(
                [
                    'Check 08 failed: '         => $booking->id,
                    'sdPaid'                    => $sdPaid,
                    'sdHeld'                    => $sdHeld,
                    'sdRefunded'                => $sdRefunded,
                    'financeCheck'              => $financeCheck,
                    'check FORMULA'             => '$row["Total Cost"] - $row["Total Relating to Holiday"] - $row["Balance Due on Holiday"]',
                    'Total Cost'                => $row['Total Cost'],
                    'Total Relating to Holiday' => $row['Total Relating to Holiday'],
                    'Balance Due on Holiday'    => $row['Balance Due on Holiday'],

                    'Booking Status' => $row['Status'],
                ]
            );
            die();
        }
    }

    public function sumFinanceChecks($row): void
    {
        $financeCheck1  = round($row['Total Cost'] - $row['Total Rent'] - $row['Booking Fee'] - $row['Extras'] - $row['Insurance'], 2);
        $financeCheck2  = round($row['Total Cost'] - $row['Total Due to owner'] - $row['Total to NCCC'], 2);
        $financeCheck3  = round($row['Total Due to owner'] - $row['Due to owner EXTRAS'] - $row['Due to owner RENT'], 2);
        $financeCheck4  = round($row['Total to NCCC'] - $row['Comm Gross'] - $row['Booking Fee'] - $row['Extras Comm Gross'] - $row['Insurance'], 2);
        $financeCheck5  = round($row['Total Rent'] - $row['Due to owner RENT'] - $row['Comm Gross'], 2);
        $financeCheck6  = round($row['Extras'] - $row['Due to owner EXTRAS'] - $row["Extras Comm Gross"], 2);
        $financeCheck7  = round($row['Total Due to owner'] - $row['Paid to owner'] - $row['OWNER LIABILITY'], 2);
        $financeCheck8  = round($row['Total Cost'] - $row['Total Relating to Holiday'] - $row['Balance Due on Holiday'], 2);
        $financeCheck9  = round($row['Property Comm%'] - $row['Comm%'], 2);
        $financeCheck10 = round($row['Paid to owner'] - $row['Paid Per Statement'], 2);

        $this->totalErrorCheck->check1 += $this->financeCheck($financeCheck1) ? 0 : 1;
        $this->totalErrorCheck->check2 += $this->financeCheck($financeCheck2) ? 0 : 1;
        $this->totalErrorCheck->check3 += $this->financeCheck($financeCheck3) ? 0 : 1;
        $this->totalErrorCheck->check4 += $this->financeCheck($financeCheck4) ? 0 : 1;
        $this->totalErrorCheck->check5 += $this->financeCheck($financeCheck5) ? 0 : 1;
        $this->totalErrorCheck->check6 += $this->financeCheck($financeCheck6) ? 0 : 1;
        $this->totalErrorCheck->check7 += $this->financeCheck($financeCheck7) ? 0 : 1;
        $this->totalErrorCheck->check8 += $this->financeCheck($financeCheck8) ? 0 : 1;


        if (! $this->financeCheck($financeCheck1)) {
            $this->bookingErrorCheck->check1[] = $row['Booking Ref'];
        }
        if (! $this->financeCheck($financeCheck2)) {
            $this->bookingErrorCheck->check2[] = $row['Booking Ref'];
        }
        if (! $this->financeCheck($financeCheck3)) {
            $this->bookingErrorCheck->check3[] = $row['Booking Ref'];
        }
        if (! $this->financeCheck($financeCheck4)) {
            $this->bookingErrorCheck->check4[] = $row['Booking Ref'];
        }
        if (! $this->financeCheck($financeCheck5)) {
            $this->bookingErrorCheck->check5[] = $row['Booking Ref'];
        }
        if (! $this->financeCheck($financeCheck6)) {
            $this->bookingErrorCheck->check6[] = $row['Booking Ref'];
        }
        if (! $this->financeCheck($financeCheck7)) {
            $this->bookingErrorCheck->check7[] = $row['Booking Ref'];
        }
        if (! $this->financeCheck($financeCheck8)) {
            $this->bookingErrorCheck->check8[] = $row['Booking Ref'];
        }

        $this->currentErrorCheck->check1 += abs($financeCheck1);
        $this->currentErrorCheck->check2 += abs($financeCheck2);
        $this->currentErrorCheck->check3 += abs($financeCheck3);
        $this->currentErrorCheck->check4 += abs($financeCheck4);
        $this->currentErrorCheck->check5 += abs($financeCheck5);
        $this->currentErrorCheck->check6 += abs($financeCheck6);
        $this->currentErrorCheck->check7 += abs($financeCheck7);
        $this->currentErrorCheck->check8 += abs($financeCheck8);

        $this->bookingsErrorCheck[$row['Booking Ref']] = (object) [
            'Check 1'  => $financeCheck1,
            'Check 2'  => $financeCheck2,
            'Check 3'  => $financeCheck3,
            'Check 4'  => $financeCheck4,
            'Check 5'  => $financeCheck5,
            'Check 6'  => $financeCheck6,
            'Check 7'  => $financeCheck7,
            'Check 8'  => $financeCheck8,
            'Check 9'  => $financeCheck9,
            'Check 10' => $financeCheck10,
        ];
    }

    public function compareFinanceChecks(): void
    {
        echo 'Error check comparison:' . PHP_EOL;

        $differenceCheck1 = round(abs($this->currentErrorCheck->check1) - $this->lastErrorCheck->check1, 2);
        $differenceCheck2 = round(abs($this->currentErrorCheck->check2) - $this->lastErrorCheck->check2, 2);
        $differenceCheck3 = round(abs($this->currentErrorCheck->check3) - $this->lastErrorCheck->check3, 2);
        $differenceCheck4 = round(abs($this->currentErrorCheck->check4) - $this->lastErrorCheck->check4, 2);
        $differenceCheck5 = round(abs($this->currentErrorCheck->check5) - $this->lastErrorCheck->check5, 2);
        $differenceCheck6 = round(abs($this->currentErrorCheck->check6) - $this->lastErrorCheck->check6, 2);
        $differenceCheck7 = round(abs($this->currentErrorCheck->check7) - $this->lastErrorCheck->check7, 2);
        $differenceCheck8 = round(abs($this->currentErrorCheck->check8) - $this->lastErrorCheck->check8, 2);

        var_export([
            'Last error check'    => [
                'check1'    => $this->financeCheck($this->lastErrorCheck->check1) ? 'pass' : $this->lastErrorCheck->check1,
                'check2'    => $this->financeCheck($this->lastErrorCheck->check2) ? 'pass' : $this->lastErrorCheck->check2,
                'check3'    => $this->financeCheck($this->lastErrorCheck->check3) ? 'pass' : $this->lastErrorCheck->check3,
                'check4'    => $this->financeCheck($this->lastErrorCheck->check4) ? 'pass' : $this->lastErrorCheck->check4,
                'check5'    => $this->financeCheck($this->lastErrorCheck->check5) ? 'pass' : $this->lastErrorCheck->check5,
                'check6'    => $this->financeCheck($this->lastErrorCheck->check6) ? 'pass' : $this->lastErrorCheck->check6,
                'check7'    => $this->financeCheck($this->lastErrorCheck->check7) ? 'pass' : $this->lastErrorCheck->check7,
                'check8'    => $this->financeCheck($this->lastErrorCheck->check8) ? 'pass' : $this->lastErrorCheck->check8,
                'checkDate' => $this->lastErrorCheck->checkDate,
            ],
            'Current error check' => [
                'check1'    => $this->financeCheck($this->currentErrorCheck->check1) ? 'pass' : round($this->currentErrorCheck->check1, 2),
                'check2'    => $this->financeCheck($this->currentErrorCheck->check2) ? 'pass' : round($this->currentErrorCheck->check2, 2),
                'check3'    => $this->financeCheck($this->currentErrorCheck->check3) ? 'pass' : round($this->currentErrorCheck->check3, 2),
                'check4'    => $this->financeCheck($this->currentErrorCheck->check4) ? 'pass' : round($this->currentErrorCheck->check4, 2),
                'check5'    => $this->financeCheck($this->currentErrorCheck->check5) ? 'pass' : round($this->currentErrorCheck->check5, 2),
                'check6'    => $this->financeCheck($this->currentErrorCheck->check6) ? 'pass' : round($this->currentErrorCheck->check6, 2),
                'check7'    => $this->financeCheck($this->currentErrorCheck->check7) ? 'pass' : round($this->currentErrorCheck->check7, 2),
                'check8'    => $this->financeCheck($this->currentErrorCheck->check8) ? 'pass' : round($this->currentErrorCheck->check8, 2),
                'checkDate' => $this->currentErrorCheck->checkDate,
            ],
            'Difference'          => [
                'check1' => $this->financeCheck($differenceCheck1) ? 'no difference' : $differenceCheck1,
                'check2' => $this->financeCheck($differenceCheck2) ? 'no difference' : $differenceCheck2,
                'check3' => $this->financeCheck($differenceCheck3) ? 'no difference' : $differenceCheck3,
                'check4' => $this->financeCheck($differenceCheck4) ? 'no difference' : $differenceCheck4,
                'check5' => $this->financeCheck($differenceCheck5) ? 'no difference' : $differenceCheck5,
                'check6' => $this->financeCheck($differenceCheck6) ? 'no difference' : $differenceCheck6,
                'check7' => $this->financeCheck($differenceCheck7) ? 'no difference' : $differenceCheck7,
                'check8' => $this->financeCheck($differenceCheck8) ? 'no difference' : $differenceCheck8,
            ],
        ]) . PHP_EOL;
    }

    public function appendDataToErrorCheck($fileName): void
    {
        $fileStream = fopen($fileName, 'ab');

        if (! $fileStream) {
            echo PHP_EOL . 'ERROR - Could not open file "' . $fileName . '"' . PHP_EOL;
            return;
        }

        // Write data
        fputcsv($fileStream, get_object_vars($this->currentErrorCheck));

        fclose($fileStream);
    }

    public function outputErrorReport(): void
    {
        if (! empty(FINANCE_CHECK)) {
            var_export($this->totalErrorCheck);
            var_export($this->bookingErrorCheck->FINANCE_CHECK);
        } else {
            var_export($this->totalErrorCheck);
            var_export($this->bookingErrorCheck);
        }
    }

    public function printBooking($booking, $row): void
    {
        if (! empty(FINANCE_CHECK) && $booking->id == FINANCE_CHECK && ! in_array(FINANCE_CHECK, FINANCE_CHECK_LIST, true)) {
            echo 'booking_array' . PHP_EOL;
            print_r($booking);
            echo PHP_EOL . 'CSV Row (goes on the file)';
            print_r($row);
            echo PHP_EOL . '--------------------' . PHP_EOL . '--------------------' . PHP_EOL . '--------------------' . PHP_EOL;
        }
    }

    public function checkBooking($knownBookings, $booking, $row): void
    {
        if (isset($knownBookings[$booking->id]) && ! in_array($booking->id, DODGY_BOOKINGS, true)) {
            $checkColumns   = $knownBookings[$booking->id];
            $currentBooking = array_combine(HEADERS_FILTERED_BOOKINGS, $row);
            $checkArray     = array_intersect_key($currentBooking, $checkColumns);
            $diff           = array_diff($checkColumns, $checkArray);
            if ($diff) {
                echo PHP_EOL . "Booking $booking->id" . PHP_EOL;
                foreach ($diff as $key => $val) {
                    echo $key . " : " . "{'+':" . $checkColumns[$key] . ", '-':" . $checkArray[$key] . "}" . PHP_EOL;
                }
            }
        }
    }

    public function getBookingsErrorCheck(): array
    {
        return $this->bookingsErrorCheck;
    }
}
