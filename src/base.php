<?php

use Carbon\Carbon;

include_once 'Constants.php';
include_once 'FinanceChecks.php';
include_once 'Helpers.php';
include_once 'repository.php';
require __DIR__ . '/../vendor/autoload.php';

validateScriptInput([DUMP_DATE]);

$conn           = initDB();
$financeChecks  = new FinanceChecks();
$outputFileName = 'filtered-booking-' . DUMP_DATE . '-Dump-' . BRAND_NAME . '.csv';
$out            = null;

if (empty(FINANCE_CHECK)) {
    $out = openFile(EXPORT_DIRECTORY . '/' . DUMP_DATE . '/', $outputFileName, 'wb');
    addDescriptions($out, FINANCE_CHECK_HEADER_DESCRIPTIONS, 46);
    addHeaders($out, HEADERS_FILTERED_BOOKINGS, FINANCE_CHECK_HEADERS);
}

$knownFile = openFile(IMPORT_DIRECTORY . '/', KNOWN_BOOKINGS_FILE, 'rb');
$headers   = fgetcsv($knownFile);
$known     = [];

while (($row = fgetcsv($knownFile)) !== false) {
    $row                        = array_combine($headers, $row);
    $known[$row['Booking Ref']] = $row;
}

$bookings         = getAllBookings($conn);
$customerPayments = getCustomerPayments($conn, $bookings);
$bookingsExtras   = getAllExtras($conn, $bookings);
$ownerPayments    = getAllOwnerPayments($conn, $bookings);
$securityDeposits = getAllSecurityDeposits($conn, $bookings);