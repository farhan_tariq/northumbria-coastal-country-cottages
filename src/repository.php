<?php

function getAllBookings($conn): array
{
    $bookings = [];
    $where    = '';

    if (! empty(FINANCE_CHECK) && ! in_array(FINANCE_CHECK, FINANCE_CHECK_LIST)) {
        $where .= ' AND b.booking_id = ' . FINANCE_CHECK;
    }

    $sql = "
            SELECT `b`.`booking_id` AS id,
                   `b`.`_fk_property`,
                   `b`.`_fk_customer`,
                   `b`.`booked_date`,
                   `b`.`from_date`,
                   `b`.`to_date`,
                   `b`.`status`,
                   `b`.source,
                   `b`.`booking_type`,
                   `b`.`total_price`,
                   `bd`.bookingfullrate AS actual_rent,
                   `b`.`rental_price` AS adjusted_rent,
                   `bd`.`bookingdiscount_couples` + `bd`.bookingdiscount_early + `bd`.bookingdiscount_late + `bd`.bookingdiscount_multi + `bd`.bookingdiscount_oap + `bd`.bookingdiscount_spare + `bd`.bookingdiscount_xy + `bd`.bookingfixeddiscount + `bd`.bookingotherdiscount AS discount,
                   `b`.`voucher`,
                   `b`.`booking_fee_price` AS booking_fee,
                   `b`.`credit_card_fee_amount`,
                   `b`.`extras_price`,
                   `b`.`extras_commission`, /*  Always the same amount for all bookings*/
                   `b`.`commission_rate`,
                   `b`.`due_to_owner`,
                   `b`.`paid_to_owner`,
                   `b`.`outstanding_to_owner`,
                   `b`.`security_deposit_amount`,
                   `b`.`security_deposit_paid`,
                   `b`.`security_deposit_due_date`,
                   `b`.`cancelled_date`,
                   `bd`.cancelled_bookingfullrate,
                   `p`.commission_percentage AS property_commission_percentage,
                   `p`.name AS property_name,
                   `p`.rcvoldID AS property_unique_ref,
                   `p`._fk_owner,
                   `b`.`comment`,
                   `bd`.`cleanersnotes`
            FROM `booking` b
                     JOIN `booking_details` `bd` ON `b`.__pk = bd.bookingID
                     JOIN property p ON p.__pk = b._fk_property 
            WHERE b.`from_date` >= '" . FINANCIAL_YEAR_START_DATE . "' {$where} ORDER BY b.booking_id ;";

    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetch_object()) {
            $bookings[$obj->id]                                 = $obj;
            $bookings[$obj->id]->total_received_from_customer   = 0;
            $bookings[$obj->id]->total_refunded_to_customer     = 0;
            $bookings[$obj->id]->security_deposit_paid_amount   = 0;
            $bookings[$obj->id]->security_deposit_refunded      = 0;
            $bookings[$obj->id]->owner_payments_adjusted_amount = 0;
            $bookings[$obj->id]->payments                       = [];
            $bookings[$obj->id]->extras                         = [];
            $bookings[$obj->id]->booking_fee                    = 0;
            $bookings[$obj->id]->insurance                      = 0;
            $bookings[$obj->id]->finances                       = [];
            $bookings[$obj->id]->owner_payments                 = [];
            $bookings[$obj->id]->owner_payment_adjustments      = [];
        }
    }

    echo "Fetched All Bookings" . PHP_EOL;
    return $bookings;
}

function getCustomerPayments($conn, $bookings): array
{
    $payments = [];
    $where    = '';

    if (! empty(FINANCE_CHECK) && ! in_array(FINANCE_CHECK, FINANCE_CHECK_LIST)) {
        $where .= ' AND b.booking_id = ' . FINANCE_CHECK;
    }

    $sql = "
            SELECT bp.*
            FROM booking b
                JOIN booking_payment bp ON bp.`_fk_booking` = b.`booking_id`
            WHERE b.`from_date` >= '" . FINANCIAL_YEAR_START_DATE . "' {$where};";

    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetch_object()) {

            if (! empty($bookings[$obj->_fk_booking])) {
                if ($obj->amount > 0 && $obj->date <= DUMP_DATE) {
                    if (array_stripos($obj->paymentcaption, ['securi', 'sd']) !== false) {
                        $bookings[$obj->_fk_booking]->security_deposit_paid_amount = $obj->amount;
                    } else {
                        $bookings[$obj->_fk_booking]->total_received_from_customer += $obj->amount;
                    }
                }

                if ($obj->amount < 0 && $obj->date <= DUMP_DATE) {
                    if (array_stripos($obj->paymentcaption, ['securi', 'sd']) !== false) {
                        $bookings[$obj->_fk_booking]->security_deposit_refunded = $obj->amount * -1;
                    } else {
                        $bookings[$obj->_fk_booking]->total_refunded_to_customer += $obj->amount * -1;
                    }
                }

                $bookings[$obj->_fk_booking]->payments[] = $obj;
            }
            $payments[$obj->_fk_booking][] = $obj;
        }
    }
    echo "Fetched All Payments" . PHP_EOL;
    return $payments;
}

function getAllExtras($conn, $bookings): array
{
    $extras = [];
    $where  = '';

    if (! empty(FINANCE_CHECK) && ! in_array(FINANCE_CHECK, FINANCE_CHECK_LIST)) {
        $where .= ' AND b.booking_id = ' . FINANCE_CHECK;
    }

    $sql = "
            SELECT b.booking_id, bi.* FROM booking_item bi 
                JOIN booking b on bi._fk_booking_detail_id = b.booking_detail_id
            WHERE b.`from_date` >= '" . FINANCIAL_YEAR_START_DATE . "' {$where};";

    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetch_object()) {
            if (! empty($bookings[$obj->booking_id])) {
                $bookings[$obj->booking_id]->extras[] = $obj;
            }
            $extras[$obj->booking_id][] = $obj;
        }
    }
    echo "Fetched All Bookings Extras" . PHP_EOL;
    return $extras;
}

function getAllOwnerPayments($conn, $bookings): array
{
    $ownerPayments = [];
    $where         = '';

    if (! empty(FINANCE_CHECK) && ! in_array(FINANCE_CHECK, FINANCE_CHECK_LIST)) {
        $where .= ' AND b.booking_id = ' . FINANCE_CHECK;
    }

    $sql = "
           SELECT b.booking_id AS id, opo.* FROM owner_payment_owe opo 
            JOIN booking b ON b.__pk = opo.booking_id
           WHERE b.from_date >= '" . FINANCIAL_YEAR_START_DATE . "'  {$where};";

    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetch_object()) {
            if (! empty($bookings[$obj->id])) {
                $bookings[$obj->id]->owner_payments[] = $obj;
            }
            $ownerPayments[$obj->id][] = $obj;
        }
    }
    echo "Fetched All Statement Owner Payments" . PHP_EOL;
    return $ownerPayments;
}

function getAllSecurityDeposits($conn, array $bookings): array
{
    $securityDeposits = [];
    $where            = '';

    if (! empty(FINANCE_CHECK) && ! in_array(FINANCE_CHECK, FINANCE_CHECK_LIST)) {
        $where .= ' AND b.booking_id = ' . FINANCE_CHECK;
    }

    $sql = "
           SELECT * FROM security_deposits sd 
            JOIN booking b ON b.__pk = sd.bookingID
           WHERE b.from_date >= '" . FINANCIAL_YEAR_START_DATE . "' {$where};";

    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetch_object()) {
            if (! empty($bookings[$obj->booking_id])) {
                $bookings[$obj->booking_id]->security_deposit_amount += $obj->depositamount;
            }
            $securityDeposits[$obj->booking_id][] = $obj;
        }
    }
    echo "Fetched All Bookings Security Deposits" . PHP_EOL;
    return $securityDeposits;
}

// Owner Cancellation/Compensation to ncc/ Reversal of Payments
function getManualOwnerAdjustments($conn, array $bookings): array
{
    $adjustments = [];
    $sql         = " SELECT
              p.__pk,  
              opo.`owner_id`,
              opo.`payment_date`,
              opo.`paid_date`,
              opo.`payment_caption`,
              opo.`currency`,
              opo.`commission_amount`,
              opo.`commission_vat`,
              opo.`amount`
            FROM
              owner_payment_owe opo
              JOIN property p
                ON p.`_fk_owner` = opo.`owner_id`
            WHERE  opo.`booking_id` = 0 ";

    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetch_object()) {
            preg_match('(\d{6,8})', $obj->payment_caption, $matches);

            if (! empty($matches)) {
                $merged_temp_object         = (object) array_merge(['booking_id' => $matches[0]], (array) $obj);
                $adjustments[$matches[0]][] = $merged_temp_object;
                if (! empty($booking[$matches[0]])) {
                    $bookings[$matches[0]]->owner_payment_adjustments      = $merged_temp_object;
                    $bookings[$matches[0]]->owner_payments_adjusted_amount += $obj->amount;
                }
            }
        }
    }
    echo "Fetched All Manual Owner Adjustments" . PHP_EOL;

    return $adjustments;
}